import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Created by Kate on 2016-12-02.
 */
public class Test {

    public static void main(String args[]) throws InterruptedException {
        RAID raid = new RAID();
        Disk disk[] = new Disk[6];
        for (int i = 0; i < 6; i++){
            disk[i] = new Disk(50);
        }

        raid.addDisks(disk);
        raid.write(0, 100);

        for (int i = 1; i < 100; i = i + 10){
            raid.write(i, 50);
        }

        Thread.sleep(5000);

        for (int i = 0; i < 6; i++){
//            System.out.println(disk[i].disk.toString());
//            for (int i = 0; i < 50; i++)
            System.out.println(Arrays.toString( disk[i].disk));
        }

    }

}
