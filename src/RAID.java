import java.util.LinkedList;

/**
 * Created by Kate on 2016-11-12.
 */

public class RAID implements RAIDInterface2 {
    private volatile DiskInterface2[] diskInterfaces;
    private volatile boolean isShutDown;
    private int numberOfDisks;
    private int diskSize;
    private int sumDiskNumber;
    private int checkSum;
    private volatile LinkedList<ObjectToProcess> writeQueue[];
    private volatile LinkedList<ObjectToProcess> readQueue[];
    private volatile Worker[] writeWorkers;

    private enum TypeOfRequest{
        read, write;
    }

    private class ObjectToProcess{
        TypeOfRequest type;
        int value;
        int sector;
        ObjectToProcess(TypeOfRequest type, int sector){
            this.type = type;
            this.sector = sector;
        }

        public ObjectToProcess(TypeOfRequest type, int sector, int value) {
            this.type = type;
            this.value = value;
            this.sector = sector;
        }
    }

    private class Worker extends Thread{
        private Thread thred;
        private int queueNumber;
        Worker(int number){
            queueNumber = number;
        }

        public void run(){
            int i = 0;
            ObjectToProcess objectToProcess;
            while(i < 5) {//jeśli pod rząd mamy pustą kolejkę 5 razy to wątek się zamyka. -> Czy jest tego sens? Może lepiej ubijać wątek i tworzyć nowy? Zbadać narzut na tworzenie wątków. -> wydaje mi się że >100ms
                //zrobić if czy jest coś do read, jeśli tak to sleep, jeśli nie to write woreker będzie tylko zapisywał.
                synchronized (readQueue[queueNumber]) {
                    if (!readQueue[queueNumber].isEmpty()) {
                        objectToProcess = null;
                        synchronized (writeQueue[queueNumber]) {
                            if (!writeQueue[queueNumber].isEmpty()) {
                                objectToProcess = writeQueue[queueNumber].getFirst();
                                System.out.println("ilosc w kolejce: " + writeQueue[queueNumber].size());
//                        Lists.to
//                        writeQueue[queueNumber].toString();
                            }
                        }
                        if (objectToProcess != null) {//robimy
                            i = 0;
                            if (objectToProcess.type.equals(TypeOfRequest.write)) {
                                synchronized (diskInterfaces[queueNumber]) {
                                    if(!isShutDown) {
                                        diskInterfaces[queueNumber].write(objectToProcess.sector, objectToProcess.value);
                                        writeQueue[queueNumber].removeFirst();
                                    }else{
                                        writeQueue[queueNumber].removeFirst();
                                    }
                                }
                            }
                        } else {//pusta kolejka śpimy
                            i++;
                            try {
                                Thread.sleep(50);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        public void start(){
            if(thred==null){
                thred = new Thread(this);
                thred.start();
            }
        }
    }

    public RAID() {
        isShutDown = false;
    }

    @Override
    public void addDisks(DiskInterface2[] array) {
        diskInterfaces = array;
        numberOfDisks = array.length;
        diskSize = array[0].size();//every disk is equal
        sumDiskNumber = array.length - 1;//last disk is for recovery
        checkSum = (array.length - 1) * array[0].size();//every disk is equal

        writeQueue = new LinkedList[numberOfDisks];
        readQueue = new LinkedList[numberOfDisks];
        writeWorkers = new Worker[numberOfDisks];
        for(int i = 0; i < numberOfDisks; i++) {
            writeQueue[i] = new LinkedList<>();
            readQueue[i] = new LinkedList<>();
            writeWorkers[i] = new Worker(i);
        }
    }

    @Override
    public void write(int sector, int value) {
        int diskNumber = sector / diskSize;
        int physicalSectorNumber = sector % diskSize;

        if(!isShutDown) {
            synchronized (writeQueue[diskNumber]) {
                //sprawdzić czy istnieje na koljce sektor pod który zapisujemy -> jeśli tak to update.
//            System.out.println("disk nr: " + diskNumber + " value: " + value);
                if (!updateSector(sector, value)) {//jeśli nie ma sektora w kolejce, to dodajemy go.
                    writeQueue[diskNumber].addLast(new ObjectToProcess(TypeOfRequest.write, physicalSectorNumber, value));
                }
            }
            if (!writeWorkers[diskNumber].isAlive()) {//wątek się zakończył -> trzeba zrobić nowy...
                writeWorkers[diskNumber].start();
            }
        }
    }

    @Override
    public int read(int sector) {
//        return -1;
        int diskNumber = sector / diskSize;
        int physicalSectorNumber = sector % diskSize;
        if(diskInterfaces[diskNumber] != null) {
            synchronized (readQueue[diskNumber]) {
                if(!isShutDown) {
                    readQueue[diskNumber].addLast(new ObjectToProcess(TypeOfRequest.read, sector));
                }
            }
            if (!isShutDown) {//read data from write queue, if exists
                synchronized (writeQueue[diskNumber]) {
                    for (int i = 0; i < writeQueue[diskNumber].size(); i++) {
                        if (writeQueue[diskNumber].get(i).sector == sector) {
                            int value = writeQueue[diskNumber].get(i).value;
                            synchronized (readQueue[diskNumber]) {
                                readQueue[diskNumber].removeFirst();
                            }
                            return isShutDown ? value : -1;
                        }
                    }
                }
                synchronized (diskInterfaces[diskNumber]) {
                    if(!isShutDown) {
                        int value = diskInterfaces[diskNumber].read(physicalSectorNumber);
                        synchronized (readQueue[diskNumber]) {
                            readQueue[diskNumber].removeFirst();
                        }
                        return value;
                    }else{
                        return -1;
                    }
                }
            } else {
                return -1;
            }
        } else{
            return restoreDate(physicalSectorNumber);
        }
    }

    @Override
    public int size() {
        return checkSum;
    }

    @Override
    public void shutdown() {
        isShutDown = true;
    }

    @Override
    public boolean sectorInUse(int sector) {
        int diskNumber = sector / diskSize;
        int physicalSectorNumber = sector % diskSize;
        synchronized (writeQueue[diskNumber]){
            for(int i = 0; i < writeQueue[diskNumber].size(); i++){
                if(writeQueue[diskNumber].get(i).sector == physicalSectorNumber)
                    return true;
            }
        }
        synchronized (readQueue[diskNumber]){
            for(int i = 0; i < readQueue[diskNumber].size(); i++){
                if(readQueue[diskNumber].get(i).sector == physicalSectorNumber)
                    return true;
            }
        }
        return false;
    }

    public boolean updateSector(int sector, int value) {
        int diskNumber = sector / diskSize;
        int physicalSectorNumber = sector % diskSize;
        synchronized (writeQueue[diskNumber]){
            for(int i = 0; i < writeQueue[diskNumber].size(); i++){
                if(writeQueue[diskNumber].get(i).sector == physicalSectorNumber) {
                    writeQueue[diskNumber].get(i).value = value;
                    return true;
                }
            }
        }
        return false;
    }

    private int restoreDate(int sector){
//        return -1;
        int dataRecovered = 0;
        synchronized (diskInterfaces[sumDiskNumber]) {
            if(!isShutDown) {
                dataRecovered = diskInterfaces[sumDiskNumber].read(sector);
            }else{
                return -1;
            }
        }
        for(int i = 0; i < numberOfDisks - 1; i++){
            synchronized (diskInterfaces[i]) {
                if(!isShutDown) {
                    dataRecovered -= diskInterfaces[i].read(sector);
                }else{
                    return -1;
                }
            }
        }
        return dataRecovered;
    }
}


//shutdown              \/
//sector in use         \/
//check write queue     \/
//czy restore działa?   \/