import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Kate on 2016-11-12.
 */
public class Disk implements DiskInterface2 {
    public int[] disk;
    int size;

    public static volatile AtomicInteger readCounter = new AtomicInteger(0);
    public static volatile AtomicInteger writeCounter = new AtomicInteger(0);

    public Disk(int size) {
        disk = new int[size];
        this.size = size;
    }

    @Override
    public void write(int sector, int value)  {
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        disk[sector] = value;

        writeCounter.addAndGet(1);

//        System.out.println("Writer coounter: " + writeCounter + "sector: " + sector + " value: " + value);
    }

    @Override
    public int read(int sector)  {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        readCounter.addAndGet(1);
        return disk[sector];
    }

    @Override
    public int size() {
        return size;
    }
}
